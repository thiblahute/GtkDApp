<p align="center">
  <img src="https://gitlab.com/csoriano/GtkDApp/raw/master/data/extra/gtkdapp.png" alt="flatpak + gtk + meson + dlang"/>
</p>

# GtkD App Template
This is an ideal template for a Linux app, using latest technologies. These include:
* [Flatpak](http://flatpak.org/) for application distribution, build orchestation and installation in any Linux distribution.
* [Gtk+](https://www.gtk.org/) as the UI toolkit and platform.
* [Meson](http://mesonbuild.com/) as a modern build system.
* [DLang](http://dlang.org/) as the programming language.

# Build & Installation
## Using Flatpak
You don't need anything else on your system apart of Flatpak. Not even a D compiler! Flatpak will download and build all the dependencies and the environment needed.
This includes latest stable dmd, druntime, phobos and also master git of gtkd. Builds are reproducible and the app is sanbdboxed.

It will take some time to build the first time since it will build the stack, but should be quick, around 10s max, after the first build. Building the D and gtkd stack will be avoidable once it becomes good enough to be part of the Sdk itself.

First [install Flatpak](http://flatpak.org/getting.html) in your system. Then you can continue with the instructions using what you are more comfortable with.
### [Builder IDE](https://wiki.gnome.org/Apps/Builder) integration
Follow [GNOME's newcomers guide](https://wiki.gnome.org/Newcomers/BuildProject) to set it up. The guide actually works for any flatpaked app.
Basically, put the git url in the open dialog, and everything will be automated for you.
### Helper CLI tool
You can use an experimental [CLI tool](https://gitlab.com/csoriano/flatpak-dev-cli) for automating the development explained in [Manual CLI](#manual-cli) section.
This will set up Flatpak and the build environment, and when done you will be inside the sandboxed environment ready to develop the app and run with `gtkdapp`.
### Manual CLI
If you really want to know what happens behind the scenes, you can follow these instructions.
0. Install the GNOME runtimes and sdk with:
 0. `flatpak remote-add --if-not-exists gnome-nightly https://sdk.gnome.org/gnome-nightly.flatpakrepo`
 0. `flatpak install gnome-nightly org.gnome.Platform`
 0. `flatpak install gnome-nightly org.gnome.Sdk`
0. `git clone git@gitlab.com:csoriano/GtkDApp.git`
0. Go to the code with `cd GtkDApp`
0. Build executing `flatpak-builder --repo=repo ../buildGtkDApp build-aux/flatpak/org.example.GtkDApp.json`
0. Create a local repo with `flatpak --user remote-add --no-gpg-verify --if-not-exists repo repo`
0. Install to that repo with `flatpak install repo org.example.GtkDApp --user`
0. Run with `flatpak run org.example.GtkDApp`

## Using regular host build
You can use meson build system directly, and install the dependencies, dmd and gtkd.

Then you can execute `meson build`, `ninja -C build`, `ninja -C build install`.
Then run with `gtkdapp`.

# Contribution
Just create an issue or a pull request. Copyrights are in the git repo, not need to set them on the files. License is also the one on the project, not need to specify per file.

Alternatively you can contact me on IRC at irc.gnome.org in #Nautilus channel as csoriano or CarlosSoriano.